
let $date = new Date();
let $year = $date.getFullYear();
  $('footer .year').text($year);

function openNav() {
  $('.nav').toggleClass('nav--open');
}

function closeNav() {
  document.getElementsByClassName("nav")[0].style.width = "0";
  document.getElementsByClassName("main")[0].style.marginLeft= "0";
  document.body.style.backgroundColor = "white";
}
